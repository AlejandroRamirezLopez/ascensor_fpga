----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2020 09:29:45
-- Design Name: 
-- Module Name: Decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY decoder IS
    PORT (
        code : IN std_logic_vector(3 DOWNTO 0);
        led : OUT std_logic_vector(6 DOWNTO 0)
    );
END ENTITY decoder;

ARCHITECTURE dataflow OF decoder IS
    BEGIN
        WITH code SELECT
            led <=  "1100010" WHEN "0000",--    Parte superior de P
                    "0000001" WHEN "0001",--    0
                    "1001111" WHEN "0010",--    1,I
                    "1101111" WHEN "0011",--    Parte superior de 1,I
                    "0010010" WHEN "0100",--    2
                    "1110010" WHEN "0101",--    Parte superior de S
                    "1101010" WHEN "0110",--    Parte superior de 0,O
                    "0111101" WHEN "0111",--    Parte inferior de P
                    "0000110" WHEN "1000",--    3
                    "1011111" WHEN "1001",--    Parte inferior de 1,I
                    "0011110" WHEN "1010",--    Parte inferior de S,3
                    "1011100" WHEN "1011",--    Parte inferior de O
                    "0011000" WHEN "1100",--    P
                    "1100110" WHEN "1101",--    Parte superior de 2,3
                    "0100100" WHEN "1110",--    S
                    "0111100" WHEN "1111",--    Parte inferior de 2
                    "1111110" WHEN OTHERS;
END ARCHITECTURE dataflow;
