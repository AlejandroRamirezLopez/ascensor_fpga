----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2020 14:30:30
-- Design Name: 
-- Module Name: holder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity holder is
  Port (
    clk_in             :   IN std_logic;
    bot_int_in         :   IN std_logic_vector(3 downto 0);
    Bot_ext_in       :   IN std_logic_vector(3 downto 0);
    piso_in            :   IN std_logic_vector(3 downto 0);
    bot_int_out        :   OUT std_logic_vector(3 downto 0);
    bot_ext_out      :   OUT std_logic_vector(3 downto 0)
  );
end holder;

architecture Behavioral of holder is
    signal but_int_hold: std_logic_vector(3 downto 0);
    signal but_ext_hold: std_logic_vector(3 downto 0);
begin
    process(bot_int_in, clk_in, Bot_ext_in)
    begin
        
        if rising_edge(clk_in) then
            if (bot_int_in(0) = '1') then
                but_int_hold(0) <= '1';
            elsif ((but_int_hold(0) = '1') AND (piso_in(0) = '1')) then
                but_int_hold(0) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (bot_int_in(1) = '1') then
                but_int_hold(1) <= '1';
            elsif ((but_int_hold(1) = '1') AND (piso_in(1) = '1')) then
                but_int_hold(1) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (bot_int_in(2) = '1') then
                but_int_hold(2) <= '1';
            elsif ((but_int_hold(2) = '1') AND (piso_in(2) = '1')) then
                but_int_hold(2) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (bot_int_in(3) = '1') then
                but_int_hold(3) <= '1';
            elsif ((but_int_hold(3) = '1') AND (piso_in(3) = '1')) then
                but_int_hold(3) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (Bot_ext_in(0) = '1') then
                but_ext_hold(0) <= '1';
            elsif ((but_ext_hold(0) = '1') AND (piso_in(0) = '1')) then
                but_ext_hold(0) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (Bot_ext_in(1) = '1') then
                but_ext_hold(1) <= '1';
            elsif ((but_ext_hold(1) = '1') AND (piso_in(1) = '1')) then
                but_ext_hold(1) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (Bot_ext_in(2) = '1') then
                but_ext_hold(2) <= '1';
            elsif ((but_ext_hold(2) = '1') AND (piso_in(2) = '1')) then
                but_ext_hold(2) <= '0';
            end if;
        end if;
        
        if rising_edge(clk_in) then
            if (Bot_ext_in(3) = '1') then
                but_ext_hold(3) <= '1';
            elsif ((but_ext_hold(3) = '1') AND (piso_in(3) = '1')) then
                but_ext_hold(3) <= '0';
            end if;
        end if;     
    end process;
    
    Bot_ext_in <= but_ext_hold;
    bot_int_out <= but_int_hold;
    
end Behavioral;
