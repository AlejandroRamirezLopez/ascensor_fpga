library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Clk_divider is
GENERIC (factor: integer:=60);
  Port ( 
         clk_in: IN STD_LOGIC;
         reset_in: IN STD_LOGIC;
         clk_out: OUT STD_LOGIC:='0' );
end Clk_divider;

architecture Behavioral of Clk_divider is
begin
    PROCESS(reset_in, clk_in)
        SUBTYPE count_range IS integer RANGE 0 TO FACTOR - 1;
        VARIABLE CONT: count_range:=COUNT_RANGE'HIGH;
    begin
        if reset_in = '1' then
            cont:= count_range'HIGH;
            clk_out<= '0';
        elsif rising_edge(clk_in) then
            if cont /=0 then
                clk_out<= '0';
                cont:= cont - 1;
            else 
                cont:= count_range'HIGH;
                clk_out<= '1';
            end if;
        end if;
   end PROCESS;
end Behavioral;
