----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2020 14:02:22
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top is
  generic (
    FREQIN: positive := 1e8
  );
  port (
    CLK         :   IN std_logic;
    RST         :   IN std_logic;
    BOT_INT     :   IN std_logic_vector(3 downto 0);
    BOT_EXT     :   IN std_logic_vector(3 downto 0);
    PISO        :   IN std_logic_vector(3 downto 0);
    MOTOR       :   OUT std_logic_vector(1 downto 0);
    PUERTA      :   OUT std_logic;
--    PLANTA      :   OUT std_logic_vector(3 downto 0);
    SEGMENTPISO :   OUT std_logic_vector (6 downto 0);
    DIGSELECT   :   OUT std_logic_vector (7 downto 0);
    LEDS        :   OUT std_logic_vector (15 downto 0)
  );
end top;

architecture Behavioral of top is 

    component Antirrebotes is
        port (
           clk_in	  :   in std_logic;
           btn_in	  :   in std_logic;
           btn_out	  :   out std_logic);
    end component;
    
    component Sincronizador is
        Port (
           sync_in     :   IN STD_LOGIC;
           clk_in      :   IN STD_LOGIC;
           sync_out    :   OUT STD_LOGIC
    );
    end component;
    
    component holder is
        Port (
           clk_in             :   IN std_logic;
           bot_int_in         :   IN std_logic_vector(3 downto 0);
           Bot_ext_in       :   IN std_logic_vector(3 downto 0);
           piso_in            :   IN std_logic_vector(3 downto 0);
           bot_int_out        :   OUT std_logic_vector(3 downto 0);
           bot_ext_out      :   OUT std_logic_vector(3 downto 0)
        );
    end component;
    
    component VectorToInteger is
    Port ( B0 : in STD_LOGIC;
           B1 : in STD_LOGIC;
           B2 : in STD_LOGIC;
           B3 : in STD_LOGIC;
           change : in std_logic;
           P_destino : out integer range 0 to 3 :=0);
    end component;
    
    component IntegerToVector is
    port (
         piso_int: IN integer range 0 to 3;
         piso_vect: OUT std_logic_vector(3 downto 0)
    );
    end component;  
    
    component maq_estados is
        port(
            piso_destino: IN integer range 0 to 3;
            piso_actual: IN integer range 0 to 3;
            piso_actual_salida: OUT integer range 0 to 3;
            clk_out: IN std_logic;
            reset: in std_logic;
            motor: out std_logic_vector(1 downto 0);
            puerta: out std_logic;
            change_piso : out std_logic
        );
    end component;
    
--    component Display_refresh is
--    port( 
--        clk : in  STD_LOGIC;
--		segment_number: IN std_logic_vector(6 downto 0);   
--        display_number : out  STD_LOGIC_VECTOR (6 downto 0);
--        display_selection : out  STD_LOGIC_VECTOR (7 downto 0));
--    end component;
    
--    component decoder is
--    port(
--        code : IN std_logic_vector(3 DOWNTO 0);
--        led : OUT std_logic_vector(6 DOWNTO 0)
--    );
--    end component;
    
    component Clk_divider is
    generic (
        FACTOR: integer:=100000
            );
    port (
        clk_in: IN STD_LOGIC;
        reset_in: IN STD_LOGIC;
        clk_out: OUT STD_LOGIC:='0'
    );
    end component;
    
    component AnimacionPuerta is
    Port (
        clk_in      :   in      std_logic;
        puerta_in   :   in      std_logic;
        leds_out    :   out     std_logic_vector(15 downto 0)
      );
    end component;
    
--    component AnimacionMotor is
--      Port (
--        clk_in          :   in  std_logic;
--        motor_in        :   in  std_logic_vector(1 downto 0);
--        planta_in       :   in  std_logic_vector(3 downto 0);
--        digsegment_out  :   out std_logic_vector(6 downto 0);
--        digselect_out   :   out std_logic_vector(7 downto 0)
--      );
--    end component;
    
    signal boton_int_deb: std_logic_vector(3 downto 0);
    signal boton_int_sync: std_logic_vector(3 downto 0);    
    signal boton_ext_deb: std_logic_vector(3 downto 0);
    signal boton_ext_sync: std_logic_vector(3 downto 0);   
    signal piso_deb: std_logic_vector(3 downto 0);
    signal piso_sync: std_logic_vector(3 downto 0);
    signal reset_deb: std_logic;  
    signal BOTONINT_holder: std_logic_vector(3 downto 0);
    signal BOTONEXT_holder: std_logic_vector(3 downto 0);
    signal PisoDestino: integer range 0 to 3;
    signal PisoActual: integer range 0 to 3;
    signal PisoActualSalida: integer range 0 to 3;
    signal planta2decoder: std_logic_vector (3 downto 0);
    signal change: std_logic;
    signal clk_display: std_logic;
    signal clk_AnimacionPuerta: std_logic;
    signal clk_AnimacionMotor: std_logic;
    signal piso_maq: std_logic_vector(3 downto 0);
    signal puerta2animacion: std_logic;
    signal motor2animacion: std_logic_vector(1 downto 0);
        
begin

    AntirrebotesBotonesInterior: for i in BOT_INT'range generate
        DEB_BOTONINT: Antirrebotes
        port map(
           clk_in	=> clk,
           btn_in => BOT_INT(i),
           btn_out => boton_int_deb(i) 
        );
    end generate AntirrebotesBotonesInterior;
    
    SincronizadoresBotonesInterior: for i in BOT_INT'range generate
        SYNC_BOTONINT: Sincronizador
        port map(
           sync_in => boton_int_deb(i),
           clk_in => clk,
           sync_out => boton_int_sync(i)
        );
    end generate SincronizadoresBotonesInterior;
    
    AntirrebotesBotonesExterior: for i in BOT_EXT'range generate
        DEB_BOTONEXT: Antirrebotes
        port map(
           clk_in	=> clk,
           btn_in => Bot_EXT(i),
           btn_out => boton_ext_deb(i) 
        );
    end generate AntirrebotesBotonesExterior;

    SincronizadoresBotonesExterior: for i in BOT_EXT'range generate
        SYNC_BOTONEXT: Sincronizador
        port map(
           sync_in => boton_ext_deb(i),
           clk_in => clk,
           sync_out => boton_ext_sync(i)
        );
    end generate SincronizadoresBotonesExterior;
    
    AntirrebotesPiso: for i in PISO'range generate
        DEB_PISO: Antirrebotes
        port map(
           clk_in	=> clk,
           btn_in => piso(i),
           btn_out => piso_deb(i) 
        );
    end generate AntirrebotesPiso; 
    
    SincronizadoresPiso: for i in PISO'range generate
        SYNC_PISO: Sincronizador
        port map(
           sync_in => piso_deb(i),
           clk_in => clk,
           sync_out => piso_sync(i)
        );
    end generate SincronizadoresPiso;

    DEB_RST: Antirrebotes
    port map(
       clk_in	=> clk,
       btn_in => RST,
       btn_out => reset_deb 
    );

    ClockDisplay_refresh: Clk_divider
        GENERIC MAP (
            FACTOR => 10000
            )
        PORT MAP(
            clk_in => clk,
            reset_in => reset_deb,
            clk_out => clk_display
        );

    instHOLDER: holder
    port map(
       clk_in => clk,
       bot_int_in => boton_int_sync,
       bot_ext_in => boton_ext_sync,
       piso_in => piso_sync,
       bot_int_out => BOTONINT_holder,
       bot_ext_out => BOTONEXT_holder
    );
    
    instVectorToIntegerBOTON: VectorToInteger
    port map(
        B0 => BOTONINT_holder(0),
        B1 => BOTONINT_holder(1),
        B2 => BOTONINT_holder(2),
        B3 => BOTONINT_holder(3),
        change => change,
        P_destino => PisoDestino
    );
    
    instVectorToIntegerPISO: VectorToInteger
    port map(
        B0 => piso_sync(0),
        B1 => piso_sync(1),
        B2 => piso_sync(2),
        B3 => piso_sync(3),
        change => '1',
        P_destino => PisoActual
     );
    
    instMaquina: maq_estados
    port map(
        piso_destino => PisoDestino,
        piso_actual => PisoActual,
        piso_actual_salida => PisoActualSalida,
        clk_out => clk,
        reset => reset_deb,
        motor => MOTOR,
        puerta => puerta2animacion,
        change_piso => change
    );
    
    instIntegerToVector: IntegerToVector
    port map(
        piso_int => PisoActualSalida,
        piso_vect => planta2decoder
    );
        
    ClockAnimacionPuerta: Clk_divider
    GENERIC MAP (
        FACTOR => 9000000
    )
    PORT MAP(
        clk_in => clk,
        reset_in => RST,
        clk_out => clk_AnimacionPuerta
    );
        
    PUERTA_anim: AnimacionPuerta
    PORT MAP(
        clk_in => clk_AnimacionPuerta,
        puerta_in => puerta2animacion,
        leds_out => LEDS
    );
        
--        ClockAnimacionMotor: Clk_divider
--        GENERIC MAP (
--            FACTOR => 9000000
--            )
--        PORT MAP(
--            CLK => clk,
--            RESET => rst,
--            CLK_OUT => clk_AnimacionMotor
--        );
        
--        MOTOR_anim: AnimacionMotor
--        PORT MAP(
--            clk_in => clk_AnimacionMotor,
--            motor_in => motor2animacion,
--            planta_in => planta2decoder,
--            digsegment_out => SEGMENTPISO,
--            digselect_out => DIGSELECT
--        );
        
    MOTOR <= motor2animacion;
    PUERTA <= puerta2animacion;

end Behavioral;
