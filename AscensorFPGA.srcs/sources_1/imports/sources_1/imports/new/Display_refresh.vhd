----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.01.2020 11:05:45
-- Design Name: 
-- Module Name: Display_refresh - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Display_refresh is
    Port (
        clk : in  STD_LOGIC;
		segment_POS1: IN std_logic_vector(6 downto 0);
		segment_POS2: IN std_logic_vector(6 downto 0);
		segment_POS3: IN std_logic_vector(6 downto 0);
		segment_POS4: IN std_logic_vector(6 downto 0);
		segment_POS5: IN std_logic_vector(6 downto 0);   
        display_number : out  STD_LOGIC_VECTOR (6 downto 0);
        display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
    );
end Display_refresh;

architecture Behavioral of Display_refresh is
begin
    muestra_displays:process (clk, segment_POS1, segment_POS2, segment_POS3, segment_POS4, segment_POS5)
	variable cnt:integer range 0 to 7;
	begin
		if (clk'event and clk='1') then 
			if cnt=7 then
				cnt:=0;
			else
				cnt:=cnt+1;
			end if;
		end if;	
		case cnt is
				when 0 => display_selection <= "11111110";
						    display_number <= segment_POS5;				
				when 1 => display_selection <= "11111111";
						    display_number <= (others => '0');				
				when 2 => display_selection <= "11111111";
						    display_number <= (others => '0');				
				when 3 => display_selection <= "11111111";
						    display_number <= (others => '0');
			    when 4 => display_selection <= "11101111";
						    display_number <= segment_POS4;
				when 5 => display_selection <= "11011111";
						    display_number <= segment_POS3;
				when 6 => display_selection <= "10111111";
						    display_number <= segment_POS2;		    
				when 7 => display_selection <= "01111111";
						    display_number <= segment_POS1;		    		    				
			end case;
	end process;
end Behavioral;
