library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity maq_estados is
PORT (
    piso_destino: IN integer range 0 to 3;
    piso_actual: IN integer range 0 to 3;
    piso_actual_salida: OUT integer range 0 to 3;
    clk_out: IN std_logic;
    reset: in std_logic;
    motor: out std_logic_vector(1 downto 0);
    puerta: out std_logic;
    change_piso : out std_logic --va al selec_piso para que no cambie
                                --cuando el motor esta encendido 
   -- estado: out integer -- S�lo de prueba para ver m�s claramente
                        -- en la simulacion si cambia de estado 
    );
    
end maq_estados;

architecture Behavioral of maq_estados is

TYPE state_type IS (S0,S1,S2,S3);
SIGNAL state: state_type;
signal next_state: state_type;
--SIGNAL piso_actual: INTEGER RANGE 0 TO 3:=0;

begin

NEXT_STATE_DECODE: PROCESS (PISO_DESTINO,PISO_ACTUAL,CLK_OUT,RESET,STATE)



BEGIN
	next_state <=S0; --Por defecto S0
    
    CASE (state) is

        WHEN S0 =>

            IF (PISO_DESTINO=piso_actual) THEN
                next_state<=S0;
            ELSE
                next_state<=s1;
            END IF;

        WHEN S1 =>

            IF(PISO_DESTINO<piso_actual) THEN
	           next_state<=S2;
            ELSIF (PISO_DESTINO>piso_actual) THEN
	           next_state<=S3;
            ELSIF (PISO_DESTINO=piso_actual) THEN
	           next_state<=S0;
            END IF;

        WHEN S2=>
           --Bajando
           
            IF (PISO_DESTINO=piso_actual) THEN
	           next_state<=S1;
            ELSE
	           next_state<=S2;
            END IF;
          

        WHEN S3=>
            --Subiendo
            
            IF (PISO_DESTINO=piso_actual) THEN
	           next_state<=S1;
            ELSE
	           next_state<=S3;
            END IF;

        WHEN OTHERS => next_state<=S0;

    END CASE;

    piso_actual_salida<=piso_actual;
END PROCESS;


OUTPUT_DECODE: PROCESS (state)
BEGIN
    CASE (state) IS
        WHEN S0 =>
	       MOTOR<="00";
	       PUERTA <='1';
	       CHANGE_PISO<='1';
	       --estado <=0;
        WHEN S1 =>
	       MOTOR<="00";
	       PUERTA <='0';
	       CHANGE_PISO<='0';
	       --estado <=1;
        WHEN S2 =>
	       MOTOR<="01"; --Bajando
	       PUERTA <='0';
	       CHANGE_PISO<='0';
	       --estado <=2;
        WHEN S3 =>
	       MOTOR<="10"; --Subiendo
	       PUERTA <='0';
	       CHANGE_PISO<='0';
	       --estado <=3;
        WHEN OTHERS => 
            MOTOR<="00";
	       PUERTA <='1';
	       CHANGE_PISO<='1';
	       --estado <=0;

    END CASE;
    
END PROCESS;

SYNC_PROC_DECODE: PROCESS (CLK_OUT)
BEGIN
    IF rising_edge(CLK_OUT) THEN
        IF(RESET='1') THEN
            state<=S0;
        ELSE
            state<=next_state;
        END IF;
    END IF;
END PROCESS;

--SYNC_PROC_PISO_ACTUAL: PROCESS(piso_actual,state,clk_out)
--BEGIN
--    if rising_edge(clk_out) then
--         IF (piso_actual/=piso_destino) THEN
--            IF (state = S2) THEN
--                 piso_actual<=piso_actual-1;
--             END IF;
--            IF (state = S3) THEN
--                piso_actual<=piso_actual+1;
--            END IF;
--        END IF;
--   END IF;
      
--END PROCESS;


end Behavioral;
