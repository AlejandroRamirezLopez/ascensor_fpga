----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.02.2020 18:27:43
-- Design Name: 
-- Module Name: AnimacionPuerta - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AnimacionPuerta is
  Port (
    clk_in      :   in      std_logic;
    puerta_in   :   in      std_logic;
    leds_out    :   out     std_logic_vector(15 downto 0)
  );
end AnimacionPuerta;

architecture Behavioral of AnimacionPuerta is
    signal count8_abrir: natural := 0;
    signal count8_cerrar: natural := 9;
    signal count3_abrir: natural := 0;
    signal count3_cerrar: natural := 0;
    signal cadena: unsigned(7 downto 0) := "11111111";

begin
    process(clk_in)
    begin
        if rising_edge(clk_in) then
            if (puerta_in = '1') then
                count3_cerrar <= 0;
                if (count3_abrir < 3) then
                    if (count8_abrir < 9) then
                        leds_out(15 downto 8) <= std_logic_vector(cadena sll count8_abrir);
                        leds_out(7 downto 0) <= std_logic_vector(cadena srl count8_abrir);
                        count8_abrir <= count8_abrir + 1;
                    elsif (count8_abrir = 9) then
                        count8_abrir <= 0;
                        count3_abrir <= count3_abrir + 1;
                    end if;
                elsif (count3_abrir = 3) then
                        leds_out <= (OTHERS => '0');
                end if;
            elsif (puerta_in = '0') then
                count3_abrir <= 0;
                if (count3_cerrar < 3) then
                    if (count8_cerrar > 0) then
                        leds_out(15 downto 8) <= std_logic_vector(cadena sll (count8_cerrar - 1));
                        leds_out(7 downto 0) <= std_logic_vector(cadena srl (count8_cerrar - 1));
                        count8_cerrar <= count8_cerrar - 1;
                    elsif (count8_cerrar = 0) then
                        count8_cerrar <= 9;
                        count3_cerrar <= count3_cerrar + 1;
                    end if;
                elsif (count3_cerrar = 3) then
                        leds_out <= (OTHERS => '1');
                end if;
            end if;
        end if;
    end process;
end Behavioral;
