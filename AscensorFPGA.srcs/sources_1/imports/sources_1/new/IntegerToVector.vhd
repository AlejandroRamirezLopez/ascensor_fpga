----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.02.2020 19:02:48
-- Design Name: 
-- Module Name: IntegerToVector - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IntegerToVector is
  Port ( 
    piso_int: IN integer range 0 to 3;
    piso_vect: OUT std_logic_vector(3 downto 0)
  );
end IntegerToVector;

architecture dataflow of IntegerToVector is
begin
    with piso_int select
        piso_vect <= "0001" WHEN 0,
                     "0010" WHEN 1,
                     "0100" WHEN 2,
                     "1000" WHEN 3,
                     "0000" WHEN OTHERS;


end dataflow;
