----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.02.2020 16:13:54
-- Design Name: 
-- Module Name: AnimacionMotor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AnimacionMotor is
  Port (
    clk_in          :   in  std_logic;
    motor_in        :   in  std_logic_vector(1 downto 0);
    planta_in       :   in  std_logic_vector(3 downto 0);
    digsegment_out  :   out std_logic_vector(6 downto 0);
    digselect_out   :   out std_logic_vector(7 downto 0)
  );
end AnimacionMotor;

architecture Behavioral of AnimacionMotor is

    type array_code is array (1 to 5) of std_logic_vector (3 downto 0);
--    type array_segment is array (1 to 16) of std_logic_vector (6 downto 0);
    type array_number is array (1 to 5) of std_logic_vector (6 downto 0);

    component Display_refresh is
    port( 
        clk : in  STD_LOGIC;
		segment_POS1: IN std_logic_vector(6 downto 0);
		segment_POS2: IN std_logic_vector(6 downto 0);
		segment_POS3: IN std_logic_vector(6 downto 0);
		segment_POS4: IN std_logic_vector(6 downto 0);
		segment_POS5: IN std_logic_vector(6 downto 0);   
        display_number : out  STD_LOGIC_VECTOR (6 downto 0);
        display_selection : out  STD_LOGIC_VECTOR (7 downto 0));
    end component;
    
    component decoder is
    port(
        code : IN std_logic_vector(3 DOWNTO 0);
        led : OUT std_logic_vector(6 DOWNTO 0)
    );
    end component;
    
    signal segment_num: array_number;
    signal codes: array_code ;
    
begin

        DecodersGenerate: for i in array_number'range generate
            DecoderSegment: decoder
            PORT MAP(    
            code => codes(i),
            led => segment_num(i)
            );
        end generate DecodersGenerate;
        
        DISPLAY: Display_refresh
        PORT MAP(
            clk => clk_in,
            segment_POS1 => segment_num(1),
            segment_POS2 => segment_num(2),
            segment_POS3 => segment_num(3),
            segment_POS4 => segment_num(4),
            segment_POS5 => segment_num(5),   
            display_number => digsegment_out,
            display_selection => digselect_out
        );
        
    process(clk_in)
    variable count: integer := 0;
    begin
        if rising_edge(clk_in) then
            case motor_in is
                when "00" =>  --Parte Normal            
                              codes(1) <= "1100";
                              codes(2) <= "0010";
                              codes(3) <= "1110";
                              codes(4) <= "0001";
                              codes(5) <= planta_in;          
                                  
                when "01" =>    if (count < 3) then
                                    case count is
                                        --Parte Superior
                                        when 0 =>   codes(1) <= "0000";
                                                    codes(2) <= "0011";
                                                    codes(3) <= "0101";
                                                    codes(4) <= "0110";
                                                    case planta_in is
                                                        when "0001" => codes(5) <= "0110";
                                                        when "0010" => codes(5) <= "0011";
                                                        when "0100" => codes(5) <= "1101";
                                                        when "1000" => codes(5) <= "1101";
                                                    end case;
                    
                                        --Parte Normal            
                                        when 1 =>   codes(1) <= "1100";
                                                    codes(2) <= "0010";
                                                    codes(3) <= "1110";
                                                    codes(4) <= "0001";
                                                    codes(5) <= planta_in;
                                        
                                        --Parte Inferior            
                                        when 2 =>   codes(1) <= "0111";
                                                    codes(2) <= "1001";
                                                    codes(3) <= "1010";
                                                    codes(4) <= "1011";
                                                    case planta_in is
                                                        when "0001" => codes(5) <= "1011";
                                                        when "0010" => codes(5) <= "1001";
                                                        when "0100" => codes(5) <= "1111";
                                                        when "1000" => codes(5) <= "1010";
                                                    end case;           
                                    end case;
                                    count := count +1;
                                elsif (count = 3) then
                                    count := 0;
                                end if;
                                
                when "10" =>    if (count < 3) then
                                    case count is
                                        --Parte Superior
                                        when 0 =>   codes(1) <= "0000";
                                                    codes(2) <= "0011";
                                                    codes(3) <= "0101";
                                                    codes(4) <= "0110";
                                                    case planta_in is
                                                        when "0001" => codes(5) <= "0110";
                                                        when "0010" => codes(5) <= "0011";
                                                        when "0100" => codes(5) <= "1101";
                                                        when "1000" => codes(5) <= "1101";
                                                    end case;
                    
                                        --Parte Normal            
                                        when 1 =>   codes(1) <= "1100";
                                                    codes(2) <= "0010";
                                                    codes(3) <= "1110";
                                                    codes(4) <= "0001";
                                                    codes(5) <= planta_in;
                                        
                                        --Parte Inferior            
                                        when 2 =>   codes(1) <= "0111";
                                                    codes(2) <= "1001";
                                                    codes(3) <= "1010";
                                                    codes(4) <= "1011";
                                                    case planta_in is
                                                        when "0001" => codes(5) <= "1011";
                                                        when "0010" => codes(5) <= "1001";
                                                        when "0100" => codes(5) <= "1111";
                                                        when "1000" => codes(5) <= "1010";
                                                    end case;           
                                    end case;
                                    count := count +1;
                                elsif (count = 3) then
                                    count := 0;
                                end if;
            end case;
        end if;
    end process;
end Behavioral;
