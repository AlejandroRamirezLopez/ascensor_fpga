library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity VectorToInteger is
    Port ( B0 : in STD_LOGIC;
           B1 : in STD_LOGIC;
           B2 : in STD_LOGIC;
           B3 : in STD_LOGIC;
           change : in std_logic;--evita que cambie cuando 
                                 --el motor esta encendido
           P_destino : out integer range 0 to 3 :=0);
end VectorToInteger;

architecture Behavioral of VectorToInteger is
signal error: std_logic;
signal piso: integer range 0 to 3;--Piso de destino, se�al interna
signal b_puls: std_logic_vector (3 downto 0);--vector que indica que botones est�n pulsados ('1')
begin
b_puls(0)<=B0;
b_puls(1)<=B1;
b_puls(2)<=B2;
b_puls(3)<=B3;

process (b_puls)
begin
 case b_puls is
 when "0001"=>
    piso <= 0;
    error <= '0'; 
 when "0010"=>
    piso <= 1;
    error <= '0'; 
  when "0100"=>
    piso <= 2;
    error <= '0';  
  when "1000"=>
    piso <= 3;
    error <= '0'; 
   when others=>
   error <= '1';  
 end case;
end process;

p_destino <= piso when ((error = '0') AND (change = '1')); 

end Behavioral;
